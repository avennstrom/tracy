#ifndef __TRACYDX11_HPP__
#define __TRACYDX11_HPP__

#if !defined TRACY_ENABLE

#define TracyDx11Context(x) nullptr
#define TracyDx11Destroy(x)
#define TracyDx11NamedZone(a,b,c) 
#define TracyDx11NamedZoneC(a,b,c,d)
#define TracyDx11Zone(a,b) 
#define TracyDx11ZoneC(a,b,c) 
#define TracyDx11Collect(a)

#define TracyDx11NamedZoneS(a,b,c,d) 
#define TracyDx11NamedZoneCS(a,b,c,d,e) 
#define TracyDx11ZoneS(a,b,c) 
#define TracyDx11ZoneCS(a,b,c,d) 

using TracyDx11Ctx = void*;

#else

#include <assert.h>
#include <stdlib.h>
#include <d3d11.h>
#include "Tracy.hpp"
#include "client/TracyProfiler.hpp"
#include "client/TracyCallstack.hpp"

namespace tracy
{

extern std::atomic<uint8_t> s_gpuCtxCounter;

class Dx11Ctx
{
    friend class Dx11CtxScope;

    enum { QueryCount = 64 * 1024 };

public:
	Dx11Ctx( ID3D11DeviceContext* deviceContext )
        : m_deviceContext( deviceContext )
        , m_context( s_gpuCtxCounter.fetch_add( 1, std::memory_order_relaxed ) )
        , m_head( 0 )
        , m_tail( 0 )
        , m_oldCnt( 0 )
    {
        assert( m_context != 255 );

		ID3D11Device* device;
		deviceContext->GetDevice( &device );

		m_query = new ID3D11Query*[QueryCount];

		D3D11_QUERY_DESC queryDesc;
		queryDesc.MiscFlags = 0;
		queryDesc.Query = D3D11_QUERY_TIMESTAMP;
		for (int i = 0; i < QueryCount; ++i)
		{
			device->CreateQuery( &queryDesc, &m_query[i] );
		}

		ID3D11Query* query;
		device->CreateQuery( &queryDesc, &query );

		int64_t tgpu;

		deviceContext->Flush();
		deviceContext->End( query );
		while ( S_OK != deviceContext->GetData( query, &tgpu, sizeof( tgpu ), D3D11_ASYNC_GETDATA_DONOTFLUSH ) )
		{
		}
		int64_t tcpu = Profiler::GetTime();

		query->Release();

		D3D11_QUERY_DESC disjointQueryDesc;
		disjointQueryDesc.MiscFlags = 0;
		disjointQueryDesc.Query = D3D11_QUERY_TIMESTAMP_DISJOINT;

		ID3D11Query* disjointQuery;
		device->CreateQuery( &disjointQueryDesc, &disjointQuery );

		deviceContext->Begin( disjointQuery );
		deviceContext->End( disjointQuery );

		D3D11_QUERY_DATA_TIMESTAMP_DISJOINT disjoint;
		while ( S_OK != deviceContext->GetData( disjointQuery, &disjoint, sizeof( disjoint ), D3D11_ASYNC_GETDATA_DONOTFLUSH ) )
		{
		}
		disjointQuery->Release();

		float period = 1000000000.0f / disjoint.Frequency;

		Magic magic;
        auto& token = s_token.ptr;
        auto& tail = token->get_tail_index();
        auto item = token->enqueue_begin<tracy::moodycamel::CanAlloc>( magic );
        MemWrite( &item->hdr.type, QueueType::GpuNewContext );
        MemWrite( &item->gpuNewContext.cpuTime, tcpu );
        MemWrite( &item->gpuNewContext.gpuTime, tgpu );
        memset( &item->gpuNewContext.thread, 0, sizeof( item->gpuNewContext.thread ) );
        MemWrite( &item->gpuNewContext.period, period );
        MemWrite( &item->gpuNewContext.context, m_context );
        MemWrite( &item->gpuNewContext.accuracyBits, uint8_t( 0 ) );

#ifdef TRACY_ON_DEMAND
        s_profiler.DeferItem( *item );
#endif

        tail.store( magic + 1, std::memory_order_release );

		device->Release();
    }

    ~Dx11Ctx()
    {
		for (int i = 0; i < QueryCount; ++i)
		{
			m_query[i]->Release();
		}
    }

    void Collect()
    {
		ZoneScopedC( Color::Red4 );

        if( m_tail == m_head ) return;

#ifdef TRACY_ON_DEMAND
        if( !s_profiler.IsConnected() )
        {
            m_head = m_tail = 0;
            return;
        }
#endif

        auto start = m_tail;
        auto end = m_head + QueryCount;
        auto cnt = ( end - start ) % QueryCount;
        while( cnt > 1 )
        {
            auto mid = start + cnt / 2;
            BOOL available = S_OK == m_deviceContext->GetData( m_query[mid % QueryCount], nullptr, 0, D3D11_ASYNC_GETDATA_DONOTFLUSH );
            if( available )
            {
                start = mid;
            }
            else
            {
                end = mid;
            }
            cnt = ( end - start ) % QueryCount;
        }

        start %= QueryCount;

        Magic magic;
        auto& token = s_token.ptr;
        auto& tail = token->get_tail_index();

        while( m_tail != start )
        {
            uint64_t time;
            m_deviceContext->GetData( m_query[m_tail], &time, sizeof(time), D3D11_ASYNC_GETDATA_DONOTFLUSH );

            auto item = token->enqueue_begin<tracy::moodycamel::CanAlloc>( magic );
            MemWrite( &item->hdr.type, QueueType::GpuTime );
            MemWrite( &item->gpuTime.gpuTime, (int64_t)time );
            MemWrite( &item->gpuTime.queryId, (uint16_t)m_tail );
            MemWrite( &item->gpuTime.context, m_context );
            tail.store( magic + 1, std::memory_order_release );
            m_tail = ( m_tail + 1 ) % QueryCount;
        }
    }

private:
    tracy_force_inline unsigned int NextQueryId()
    {
        const auto id = m_head;
        m_head = ( m_head + 1 ) % QueryCount;
        assert( m_head != m_tail );
        return id;
    }

    tracy_force_inline uint8_t GetId() const
    {
        return m_context;
    }

    ID3D11DeviceContext* m_deviceContext;
    ID3D11Query** m_query;
    uint8_t m_context;

    unsigned int m_head;
    unsigned int m_tail;
    unsigned int m_oldCnt;
};

class Dx11CtxScope
{
public:
    tracy_force_inline Dx11CtxScope( Dx11Ctx* ctx, const SourceLocationData* srcloc )
        : m_ctx( ctx )
#ifdef TRACY_ON_DEMAND
        , m_active( s_profiler.IsConnected() )
#endif
    {
#ifdef TRACY_ON_DEMAND
        if( !m_active ) return;
#endif
        const auto queryId = ctx->NextQueryId();
		ctx->m_deviceContext->End( ctx->m_query[queryId] );

        Magic magic;
        auto& token = s_token.ptr;
        auto& tail = token->get_tail_index();
        auto item = token->enqueue_begin<tracy::moodycamel::CanAlloc>( magic );
        MemWrite( &item->hdr.type, QueueType::GpuZoneBegin );
        MemWrite( &item->gpuZoneBegin.cpuTime, Profiler::GetTime() );
        MemWrite( &item->gpuZoneBegin.srcloc, (uint64_t)srcloc );
        MemWrite( &item->gpuZoneBegin.thread, GetThreadHandle() );
        MemWrite( &item->gpuZoneBegin.queryId, uint16_t( queryId ) );
        MemWrite( &item->gpuZoneBegin.context, ctx->GetId() );
        tail.store( magic + 1, std::memory_order_release );
    }

    tracy_force_inline Dx11CtxScope( Dx11Ctx* ctx, const SourceLocationData* srcloc, int depth )
        : m_ctx( ctx )
#ifdef TRACY_ON_DEMAND
        , m_active( s_profiler.IsConnected() )
#endif
    {
#ifdef TRACY_ON_DEMAND
        if( !m_active ) return;
#endif
        const auto thread = GetThreadHandle();

        const auto queryId = ctx->NextQueryId();
		ctx->m_deviceContext->End( ctx->m_query[queryId] );

        Magic magic;
        auto& token = s_token.ptr;
        auto& tail = token->get_tail_index();
        auto item = token->enqueue_begin<tracy::moodycamel::CanAlloc>( magic );
        MemWrite( &item->hdr.type, QueueType::GpuZoneBeginCallstack );
        MemWrite( &item->gpuZoneBegin.cpuTime, Profiler::GetTime() );
        MemWrite( &item->gpuZoneBegin.srcloc, (uint64_t)srcloc );
        MemWrite( &item->gpuZoneBegin.thread, thread );
        MemWrite( &item->gpuZoneBegin.queryId, uint16_t( queryId ) );
        MemWrite( &item->gpuZoneBegin.context, ctx->GetId() );
        tail.store( magic + 1, std::memory_order_release );

        s_profiler.SendCallstack( depth, thread );
    }

    tracy_force_inline ~Dx11CtxScope()
    {
#ifdef TRACY_ON_DEMAND
        if( !m_active ) return;
#endif
        const auto queryId = m_ctx->NextQueryId();
		m_ctx->m_deviceContext->End( m_ctx->m_query[queryId] );

        Magic magic;
        auto& token = s_token.ptr;
        auto& tail = token->get_tail_index();
        auto item = token->enqueue_begin<tracy::moodycamel::CanAlloc>( magic );
        MemWrite( &item->hdr.type, QueueType::GpuZoneEnd );
        MemWrite( &item->gpuZoneEnd.cpuTime, Profiler::GetTime() );
        MemWrite( &item->gpuZoneEnd.queryId, uint16_t( queryId ) );
        MemWrite( &item->gpuZoneEnd.context, m_ctx->GetId() );
        tail.store( magic + 1, std::memory_order_release );
    }

private:
	Dx11Ctx* m_ctx;

#ifdef TRACY_ON_DEMAND
    const bool m_active;
#endif
};

static inline Dx11Ctx* CreateDx11Context( ID3D11DeviceContext* deviceContext )
{
    auto ctx = (Dx11Ctx*)tracy_malloc( sizeof( Dx11Ctx ) );
    new(ctx) Dx11Ctx( deviceContext );
    return ctx;
}

static inline void DestroyDx11Context( Dx11Ctx* ctx )
{
    ctx->~Dx11Ctx();
    tracy_free( ctx );
}

}

using TracyDx11Ctx = tracy::Dx11Ctx*;

#define TracyDx11Context( ctx ) tracy::CreateDx11Context( ctx );
#define TracyDx11Destroy( ctx ) tracy::DestroyDx11Context( ctx );
#if defined TRACY_HAS_CALLSTACK && defined TRACY_CALLSTACK
#  define TracyDx11NamedZone( ctx, varname, name ) static const tracy::SourceLocationData TracyConcat(__tracy_gpu_source_location,__LINE__) { name, __FUNCTION__,  __FILE__, (uint32_t)__LINE__, 0 }; tracy::Dx11CtxScope varname( ctx, &TracyConcat(__tracy_gpu_source_location,__LINE__), TRACY_CALLSTACK );
#  define TracyDx11NamedZoneC( ctx, varname, name, color ) static const tracy::SourceLocationData TracyConcat(__tracy_gpu_source_location,__LINE__) { name, __FUNCTION__,  __FILE__, (uint32_t)__LINE__, color }; tracy::Dx11CtxScope varname( ctx, &TracyConcat(__tracy_gpu_source_location,__LINE__), TRACY_CALLSTACK );
#  define TracyDx11Zone( ctx, name ) TracyDx11NamedZoneS( ctx, ___tracy_gpu_zone, name, TRACY_CALLSTACK )
#  define TracyDx11ZoneC( ctx, name, color ) TracyDx11NamedZoneCS( ctx, ___tracy_gpu_zone, name, color, TRACY_CALLSTACK )
#else
#  define TracyDx11NamedZone( ctx, varname, name ) static const tracy::SourceLocationData TracyConcat(__tracy_gpu_source_location,__LINE__) { name, __FUNCTION__,  __FILE__, (uint32_t)__LINE__, 0 }; tracy::Dx11CtxScope varname( ctx, &TracyConcat(__tracy_gpu_source_location,__LINE__) );
#  define TracyDx11NamedZoneC( ctx, varname, name, color ) static const tracy::SourceLocationData TracyConcat(__tracy_gpu_source_location,__LINE__) { name, __FUNCTION__,  __FILE__, (uint32_t)__LINE__, color }; tracy::Dx11CtxScope varname( ctx, &TracyConcat(__tracy_gpu_source_location,__LINE__) );
#  define TracyDx11Zone( ctx, name ) TracyDx11NamedZone( ctx, ___tracy_gpu_zone, name )
#  define TracyDx11ZoneC( ctx, name, color ) TracyDx11NamedZoneC( ctx, ___tracy_gpu_zone, name, color )
#endif
#define TracyDx11Collect( ctx ) ctx->Collect();

#ifdef TRACY_HAS_CALLSTACK
#  define TracyDx11NamedZoneS( ctx, varname, name, depth ) static const tracy::SourceLocationData TracyConcat(__tracy_gpu_source_location,__LINE__) { name, __FUNCTION__,  __FILE__, (uint32_t)__LINE__, 0 }; tracy::Dx11CtxScope varname( ctx, &TracyConcat(__tracy_gpu_source_location,__LINE__), depth );
#  define TracyDx11NamedZoneCS( ctx, varname, name, color, depth ) static const tracy::SourceLocationData TracyConcat(__tracy_gpu_source_location,__LINE__) { name, __FUNCTION__,  __FILE__, (uint32_t)__LINE__, color }; tracy::Dx11CtxScope varname( ctx, &TracyConcat(__tracy_gpu_source_location,__LINE__), depth );
#  define TracyDx11ZoneS( ctx, name, depth ) TracyDx11NamedZoneS( ctx, ___tracy_gpu_zone, name, depth )
#  define TracyDx11ZoneCS( ctx, name, color, depth ) TracyDx11NamedZoneCS( ctx, ___tracy_gpu_zone, name, color, depth )
#else
#  define TracyDx11NamedZoneS( ctx, varname, name, depth ) TracyDx11NamedZone( ctx, varname, name )
#  define TracyDx11NamedZoneCS( ctx, varname, name, color, depth ) TracyDx11NamedZoneC( ctx, varname, name, color )
#  define TracyDx11ZoneS( ctx, name, depth ) TracyDx11Zone( ctx, name )
#  define TracyDx11ZoneCS( ctx, name, color, depth ) TracyDx11ZoneC( ctx, name, color )
#endif

#endif

#endif
